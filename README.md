# A test by Andrei Mihaila Botez 

## Technology stack
- Angular 5 ( Typescrypt, RXJS, Webpack, Karma/Jasmine )
- bootstrap
- Express for serving the AOT compiled code and Heroku deployment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `app/dist/` directory. Use the `-prod` flag for a production build.

## Serve the compiled build

Run node app.js. Navigate to `http://localhost:3001/`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Design/ Architecture

4 components: filter, grid(which holds the items), pagination and the main app component.
2 services: JsonService makes the json request, PagerService controls the grid elements(number, category, filtering)
Unit tests reside near each component, but not the abstract class for mocking services

## Required!!! 
Enable CORS to be able to make the requests to AWS
For example a Chrome plugin like Allow-Control-Allow-Origin: * or CORS Toggle
