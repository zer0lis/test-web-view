import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PaginationComponent } from './pagination.component';
import { FilterComponent } from '../filter/filter.component';
import { GridComponent } from '../grid/grid.component';
import { JsonService } from '../services/json.service';
import { PagerService } from '../services/pager.service';
import {
  Http,
  HttpModule,
  ResponseOptions,
  Response,
  BaseRequestOptions,
  RequestMethod,
  ConnectionBackend
} from '@angular/http';
import { HttpClientModule  } from '@angular/common/http';
import { NgbModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { AbstractMockObservableService } from '../mock.service';

class MockService extends AbstractMockObservableService {
  getJson() {
    return this;
  }
}

const mockHttpProvider = {
  deps: [ MockBackend, BaseRequestOptions ],
  useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions) => {
      return new Http(backend, defaultOptions);
  }
};

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;
  const mockService = new MockService();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule, HttpClientModule, NgbModule.forRoot() ],
      declarations: [
        PaginationComponent,
        FilterComponent,
        GridComponent
      ],
      providers: [ { provide: JsonService, useValue: mockService },
        { provide: Http, useValue: mockHttpProvider }, NgbModal ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should call getJson() after component rendering', () => {
    const service = fixture.debugElement.injector.get(JsonService);
    // Setup spy on the `getJson` method
    const spy = spyOn(service, 'getJson').and.returnValue({ subscribe: () => {} });
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  describe('SetPage', () => {
    it('should emit new filterValues if the page number is not  below -1 or bigger than total pages', () => {
      const totalPages = 2;
      spyOn(component.newItems, 'emit');

      component.setPage(1);
      expect(component.newItems.emit).toHaveBeenCalled();

      component.setPage(-1);
      expect(component.newItems.emit).toHaveBeenCalledTimes(1);

      component.setPage(3);
      expect(component.newItems.emit).toHaveBeenCalledTimes(1);
    });
  });
  // describe('getFilteredVideos()', () => {
  //   it('should call setPage function', () => {
  //     const obj = [{}];
  //     const spy = spyOn(component, 'setPage');
  //     component.getFilteredVideos(obj);
  //     expect(spy).toHaveBeenCalled();
  //   });
  // });
});
