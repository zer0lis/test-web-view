import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { JsonService } from '../services/json.service';
import { PagerService } from '../services/pager.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css'],
  providers: [JsonService, PagerService]
})
export class PaginationComponent implements OnInit {

  videos: Array<object> = [];
  pagedItems: any[];
  @Output() newItems = new EventEmitter<Array<object>>(); // batch of videos to be sent to the grid component
  // pager object for the pagination component
  pager: any = {};

  constructor(private service: JsonService, private pagerService: PagerService) { }

  ngOnInit() {
    this.service.getJson().subscribe((data: any) => {
      if (data) {
        Object.values(data).map( (value: any) => {
          this.videos.push(value);
          this.setPage(1);
        });
      } else {
        throw new Error('Sorry, no content could be found');
      }
    });
  }
  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
        return;
    }
    // get pager object from service
    this.pager = this.pagerService.getPager(this.videos.length, page);
    // get current page of items
    this.pagedItems = this.videos.slice(this.pager.startIndex, this.pager.endIndex + 1);
    // emit the pagedItems to the grid component
    this.newItems.emit(this.pagedItems);
  }

  getFilteredVideos($event: Array<object>): void {
    this.videos = $event;
    this.setPage(1);
  }
}
