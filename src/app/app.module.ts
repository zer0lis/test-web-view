// modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';

// components
import { AppComponent } from './app.component';
import { PaginationComponent } from './pagination/pagination.component';
import { GridComponent } from './grid/grid.component';
import { FilterComponent } from './filter/filter.component';

// Services
import { JsonService } from './services/json.service';
import { PagerService } from './services/pager.service';


@NgModule({
  declarations: [
    AppComponent,
    PaginationComponent,
    GridComponent,
    FilterComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [JsonService, PagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
