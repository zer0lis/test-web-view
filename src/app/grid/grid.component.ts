import { Component } from '@angular/core';
import { JsonService } from '../services/json.service';
import { PagerService } from '../services/pager.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { PaginationComponent } from '../pagination/pagination.component';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css'],
  providers: [JsonService, PagerService, NgbRatingConfig, PaginationComponent]
})
export class GridComponent {

  videos: Array<object> = [];

  constructor(private service: JsonService,
              private pagerService: PagerService,
              private modalService: NgbModal,
              private config: NgbRatingConfig,
              private pagination: PaginationComponent) {
    // customize default value of ratings
    config.max = 5;
    config.readonly = true;

  }
  getNewItems($event): void {
    this.videos = $event;
  }
  open(content) {
    this.modalService.open(content);
  }
  getFilteredVideos($event: Array<object>): void {
    this.videos = $event;
    this.pagination.setPage(1);
  }
}

