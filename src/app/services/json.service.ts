import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class JsonService {

  readonly url: string  = 'https://s3.amazonaws.com/vodassets/showcase.json';

  constructor(private http: HttpClient)  { }

  getJson(): Observable<any> {
    return this.http.get(this.url);
  }

}
