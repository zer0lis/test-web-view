import { Component, Output, EventEmitter } from '@angular/core';
import { JsonService } from '../services/json.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent {
  // TODO: Refactor using a Set instead of an Array
  filterValues = [];
  // Could have interfaces here but had no time to study the json feed or decide about what info to present
  allVideos: Array<object> = [];
  filteredVideos: Array<object> = [];
  @Output() newVideos = new EventEmitter<Array<object>>();

  constructor(private service: JsonService) { }

  addFilter(newValue: string): void {
    if (newValue) {
      if (this.filterValues.indexOf(newValue) === -1) { // avoid duplicate
        this.filterValues.push(newValue); // show what is being searched
      } else {
        return;
      }
      // search on the backend instead of client data. Content might have been updated
      this.service.getJson().subscribe( (data: any) => {
        if (data) {
          Object.values(data).map( (value: any) => {
            if (this.allVideos.length !== data.length) {
              this.allVideos.push(value); // keep a track of all videos in case the user resets filters
            }
            if (value.genres && value.headline) {
              if (value.genres.map( x => x.toLowerCase()).indexOf(newValue) !== -1 ||
                value.headline.toLowerCase().indexOf(newValue) !== -1) { // match found. Could add more checks here
                this.filteredVideos.push(value);
              }
            }
          });
          if (this.filteredVideos.length) {
            this.newVideos.emit(this.filteredVideos); // to the grid component
          }
        }
      });
      if (!this.filteredVideos.length) {
        this.newVideos.emit([]);
      }
    }
  }

  clearFilter(): void {
    if (!this.allVideos.length) {
      return; // check if a user clicks 'clear filters' without trying to filter before
    }
    this.filterValues = [];
    this.filteredVideos =  [];
    this.newVideos.emit(this.allVideos);
  }
}
