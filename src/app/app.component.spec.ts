import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { GridComponent } from './grid/grid.component';
import { FilterComponent } from './filter/filter.component';
import { PaginationComponent } from './pagination/pagination.component';
import { JsonService } from './services/json.service';
import { PagerService } from './services/pager.service';
import {
  Http,
  HttpModule,
  ResponseOptions,
  Response,
  BaseRequestOptions,
  RequestMethod,
  ConnectionBackend
} from '@angular/http';
import { HttpClientModule  } from '@angular/common/http';
import { NgbModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { AbstractMockObservableService } from './mock.service';

class MockService extends AbstractMockObservableService {
  getJson() {
    return this;
  }
}

const mockHttpProvider = {
  deps: [ MockBackend, BaseRequestOptions ],
  useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions) => {
      return new Http(backend, defaultOptions);
  }
};

describe('AppComponent', () => {
  let comp: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  const mockService = new MockService();

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [ HttpModule, HttpClientModule, NgbModule.forRoot() ],
      declarations: [
        AppComponent,
        GridComponent,
        FilterComponent,
        PaginationComponent,
      ],
      providers: [ { provide: JsonService, useValue: mockService },
      { provide: Http, useValue: mockHttpProvider }, NgbModal ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    comp    = fixture.componentInstance;

  });

  beforeEach(() => {
    const service = TestBed.get(JsonService);
    fixture = TestBed.createComponent(AppComponent);
    comp = fixture.componentInstance;
  });

  it('should create the app', async(() => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`page should have as title 'Source code'`, async(() => {
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Source code');
  }));
  it('page should render a link in <a> tag, to the bitbucket repo', async(() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('a').textContent).toContain('Source code');
    expect(compiled.querySelector('a').href).toBe('https://bitbucket.org/zer0lis/front-end-test-2');
  }));
});
